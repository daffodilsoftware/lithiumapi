# Development setup #

Things you may want to cover:

* Ruby version 2.2.2 and later

* System dependencies  lithium server

* clone this repository on your local machine
* Open command line console and run 
```
#!python

bundle install
```
 command to install required gems.

* run 
```
#!python

rails s
```
  in console to start server

* check with below URL in browser.

http://localhost:3000/api/v1/users

{
status: "success",
message: "",
http_code: 200,
data: 
{
type: "users",
list_item_type: "user",
size: 25,
items: 
[
{
type: "user",
id: "12",
login: "Jirka1111"
},
{
type: "user",
id: "11",
login: "SoloAdventurer"
},
{
type: "user",
id: "13",
login: "hinterlaWelcome"
},
{
type: "user",
id: "8",
login: "franzjozu"
},
{
type: "user",
id: "5",
login: "LiEricF"
},
{
type: "user",
id: "3",
login: "SkyeF"
},
{
type: "user",
id: "1",
login: "admin"
},
{
type: "user",
id: "10",
login: "TravelingMum"
},
{
type: "user",
id: "6",
login: "LiangY1"
},
{
type: "user",
id: "4",
login: "LI-DeAnna"
},
{
type: "user",
id: "9",
login: "tomas"
},
{
type: "user",
id: "20",
login: "zhurst"
},
{
type: "user",
id: "22",
login: "yanny"
},
{
type: "user",
id: "25",
login: "Christa1"
},
{
type: "user",
id: "21",
login: "stephenie@mightymediagroup.com.au"
},
{
type: "user",
id: "7",
login: "Digitalgodess"
},
{
type: "user",
id: "23",
login: "testbrendon1"
},
{
type: "user",
id: "26",
login: "ksni007"
},
{
type: "user",
id: "17",
login: "franzjozu"
},
{
type: "user",
id: "14",
login: "hinterlands"
},
{
type: "user",
id: "27",
login: "janavohe"
},
{
type: "user",
id: "29",
login: "lilake_api_user"
},
{
type: "user",
id: "2",
login: "LiangY"
},
{
type: "user",
id: "18",
login: "liangysso"
},
{
type: "user",
id: "28",
login: "jiri.sverak@gmail.com"
}
]
},
metadata: { }
}