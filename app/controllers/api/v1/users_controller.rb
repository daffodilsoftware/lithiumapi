class Api::V1::UsersController < ApplicationController
 before_action :setConstraints
 
 #method to create object of Lithium library class
 def setConstraints
   @lithium = Lithium.new
 end
 
 #method to fetch get_search_query_data from Lithium api using Lithium library
def index
 @querydata = "SELECT id,login,first_name FROM users"
 @users = @lithium.get_search_query_data(@querydata)
 render json: @users
end

end
