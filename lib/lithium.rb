require 'httparty'
require 'open-uri'
class Lithium
  include HTTParty
  default_timeout 2 # hard timeout after 1 second
  
  # get client_id of Lithium api from env variables
  def client_id
    ENV['client_id']
  end
    # base_uri of Lithium api
  def base_uri
    'https://api.stage.lithium.com'
  end
  
 # get tenant_id of Lithium api from env variables
  def tenant_id
    ENV['tenant_id']
  end
  
 # base path of Lithium api
  def base_path
    "/community/2.0/#{ tenant_id }/"
  end
  
  # method to encode query params
  def get_query_params(query)
	return URI::encode(query)
  end

  # method handle timeout request
  def handle_timeouts
    begin
      yield
    rescue Net::OpenTimeout, Net::ReadTimeout
      {}
    end
  end

  # method to fetch data from Lithium api in search query
  def get_search_query_data(query)
    handle_timeouts do
    query = get_query_params(query)
      url = "#{ base_uri }#{ base_path }search?q=#{ query }"  
      headers = { 
	    "client_id" => "#{ client_id }",
	    "Content-Type" => "application/json" 
	  }            
     return HTTParty.get(url, verify:true,:headers => headers)
    end
  end  
end
